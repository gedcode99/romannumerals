package com.ged.kata2.Roman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RomanNumerals2 {
	
	public RomanNumerals2()
	{
		super();
	}

	/*
	 * Method to convert an Arabic numeral to Roman numeral
	 * The Roman Numeral is to be of the modern 'Standard' representation rules
	 * The code takes the Arabic numeral and then checks for the largest and in effect determines 
	 * how many there are of that symbol appending the symbol to the resulting Roman numeral String
	 * The convention of placing a smaller denomination symbol to be subtracted from the immediately larger
	 * denomination symbol (Because only in certain cases are Roman numerals repeated 4 times).
	 * i.e. IV, IX, XC, CD, CM results in a symbol set of I, IV, V, IX, X, XL, C, CD, D, CM, M
	 * VX, LC and DM are not used because they would be unnecessary (5, 50 and 500)
	 * Returns empty string for zero, negative numbers or numbers > 3999
	 */
	public static String convertToRoman(int arabic) throws NumberNotValidException
	{
		StringBuilder result = new StringBuilder();
		int remaining = arabic;
		
		// No negative numbers or zero in Roman numerals
		if (arabic <= 0) 
		{
			throw new NumberNotValidException("Arabic number supplied was zero or less");
		}
		// No numbers greater than 3999
		if (arabic > 3999) 
		{
			throw new NumberNotValidException("Arabic number supplied was greater than 3999");
		}		
		
		while (remaining >= 1000)
		{
			remaining -= 1000;
			result.append("M");
		}
		
		if (remaining >= 900)
		{
			remaining -= 900;
			result.append("CM");
		}
		
		if (remaining >= 500)
		{
			remaining -= 500;
			result.append("D");
		}
		
		if (remaining >= 400)
		{
			remaining -= 400;
			result.append("CD");
		}
		
		while (remaining >=100)
		{
			remaining -= 100;
			result.append("C");
		}
		
		if (remaining >= 90)
		{
			remaining -= 90;
			result.append("XC");
		}
		
		if (remaining >= 50)
		{
			remaining -= 50;
			result.append("L");
		}
		
		if (remaining >= 40)
		{
			remaining -= 40;
			result.append("XL");
		}
		
		while (remaining >= 10)
		{
			remaining -= 10;
			result.append("X");
		}
		
		if (remaining >= 9)
		{
			remaining -= 9;
			result.append("IX");
		}
		
		if (remaining >= 5)
		{
			remaining -= 5;
			result.append("V");
		}
		
		if (remaining >= 4)
		{
			remaining -= 4;
			result.append("IV");
		}
		
		while (remaining >= 1)
		{
			remaining -= 1;
			result.append("I");
		}
		
	
		System.out.println("Arabic: "+arabic+" is: "+result.toString());
		return result.toString();
		
	}

	public static void main(String[] args) {
		SpringApplication.run(RomanNumerals2.class, args);
	}

}

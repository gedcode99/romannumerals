package com.ged.kata2.Roman;

/*
 * Custom exception to catch incorrect input
 */
public class NumberNotValidException extends Exception {
	 public NumberNotValidException(String errorMsg) {
	        super(errorMsg);
	    }

}

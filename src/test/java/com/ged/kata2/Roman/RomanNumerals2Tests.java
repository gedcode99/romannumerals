package com.ged.kata2.Roman;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RomanNumerals2Tests {

	
	// A parameterised test saves a lot of typing and could be made to test every single case
	// This test was used to develop the code using TDD
	
	@ParameterizedTest
	@CsvSource({"0, ''", "-1,''", "1.11,''", "1000,M", "2000,MM", "3000,MMM", "3900,MMMCM", "3500,MMMD", "3600,MMMDC", "3700,MMMDCC", "3800,MMMDCCC", "2850,MMDCCCL", "2840,MMDCCCXL", "2830,MMDCCCXXX", "2810,MMDCCCX", "2829,MMDCCCXXIX",
		"2825,MMDCCCXXV", "2824,MMDCCCXXIV", "2823,MMDCCCXXIII", "3949,MMMCMXLIX","999,CMXCIX","989,CMLXXXIX","949,CMXLIX",
		"49,XLIX","24,XXIV","19,XIX","14,XIV","13,XIII","9,IX","4,IV","3,III","1,I","3999,MMMCMXCIX","499,CDXCIX", "3494,MMMCDXCIV", "4000,''",  "0,''"})
	public void parameterizedTest(String input, String expected) {
		try {
			assertEquals(expected , RomanNumerals2.convertToRoman(Integer.parseInt(input)));
		} catch ( NumberFormatException | NumberNotValidException e) {
			System.out.println("Input is not valid: "+input+", Msg: "+e.getMessage());
			//e.printStackTrace();
		}
	}
}
